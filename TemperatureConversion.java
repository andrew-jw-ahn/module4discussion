/********
    This program performs Fehrenheit->Celsius and Celsisus->Fehrenheit
    temperature conversions.
********/

import java.util.Scanner;

public class TemperatureConversion
{
    public static void main( String [] args )
    {
        int userChoice              = 0; // User selection: 1, 2, 3
        double temperatureFahrenheit = 0; // Fahrenheit temperature
        double temperatureCelsius    = 0; // Celsius temperature


        // Create a Scanner to obtain user input
        Scanner input = new Scanner( System.in );

        while (userChoice != 3)
        {
            System.out.print(
                "Enter 1 to convert F->C, 2 to convert C->F, 3 to quit: " );
            userChoice = input.nextInt();  // Read user input

            switch (userChoice)
            {
                case 1: // Convert Fahrenheit to Celsius
                {
                    System.out.print( "Enter a Fahrenheit temperature: " );
                    temperatureFahrenheit = input.nextDouble();
                    temperatureCelsius    = convertTemperature(
                                                temperatureFahrenheit,
                                                userChoice);
                    System.out.println(temperatureFahrenheit +
                        " degrees Fahrenheit is " + temperatureCelsius +
                        " degrees Celsius");
                    break;
                }

                case 2: // Convert Celsius to Fahrenheit
                {
                    System.out.print("Enter a Celsius temperature: ");
                    temperatureCelsius = input.nextDouble();
                    temperatureFahrenheit = convertTemperature(
                                                temperatureCelsius,
                                                userChoice);
                    System.out.println( temperatureCelsius +
                        " degrees Celsius is " + temperatureFahrenheit +
                        " degrees Fahrenheit" );
                    break;
                }

                case 3: // End Program
                {
                    System.out.println( "Bye Bye" );
                    break;
                }

                default: // Invalid Data Entered
                {
                    System.out.println(
                        "Invalid Data: You must enter 1, 2, or 3" );           
                    break;
                }
            } // End switch (userChoice).
        } // End while (userChoice != 3).
    }

    public static double convertTemperature(double inputTemperature,
                                                int conversionChoice)
    {
        double outputTemperature;
        if (conversionChoice == 1)
        {
            outputTemperature = (5 * (inputTemperature - 32))/ 9;    
        }
        else
        {
            outputTemperature = ((inputTemperature * 1.8) + 32);
        }
        return outputTemperature;
    }
}